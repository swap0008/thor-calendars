import { pick } from 'lodash';
import * as randomColor from 'randomcolor';
import { credentials } from '../Credentials.js';
import { CalendarModel } from '../models/CalendarModel.js';
import { CalendarEventModel } from '../models/CalendarEventModel.js';
import {
    getEventSchema,
    saveEventSchema,
    deleteEventSchema,
    updateEventSchema,
    getCalendarSchema,
    saveCalendarSchema,
    deleteCalendarSchema,
    updateCalendarSchema
} from '../schema/schema.js';
import Axios from 'axios';
import { getOAuthServiceURL } from 'services-mapping-nodejs';
import {
    CAN_ACCESS_PLATFORM_CALENDARS,
    CAN_ACCESS_GOOGLE_CALENDARS
} from '../constants/constants.js';

const Calendar = new CalendarModel();
const CalendarEvent = new CalendarEventModel();

const response = (statusCode, body) => {
    return {
        statusCode: statusCode || 200,
        body: JSON.stringify(body)
    };
}

const mapToTuiEvent = (event, calendarId) => {
    return {
        id: event.id,
        calendarId: calendarId,
        start: event.start.dateTime,
        end: event.end.dateTime,
        title: event.summary,
        location: event.location || '',
        category: 'time',
        attendees: event.attendees || []
    };
}

const mapToGoogleEvent = (event) => {
    return {
        summary: event.title,
        location: event.location,
        end: {
            dateTime: new Date(event.end._date).toISOString()
        },
        start: {
            dateTime: new Date(event.start._date).toISOString()
        },
        attendees: event.attendees || []
    };
}

const mapToTuiCalendar = (calendar, type) => {
    return {
        id: `${calendar.id}:-:${type}`,
        name: calendar.summary,
        bgColor: calendar.backgroundColor || 'blue',
        color: calendar.foregroundColor || 'white',
        description: calendar.description || ''
    };
}

const getCalendarColor = () => {
    return {
        backgroundColor: randomColor({ luminosity: 'bright' }),
        foregroundColor: 'black'
    }
}

const commonChecks = async (body: object, callback: any) => {
    const { account_id, accounts, apiId }: any = body;

    const { data }: any = Axios.post(getOAuthServiceURL(apiId), {
        account_id,
        accounts,
        type: 'CALENDAR_SERVICE'
    });

    return data;
}

//Events Helpers
const getEventsList = async (service, calendar_id) => {
    const { data: { items } } = await service.events.list({
        calendarId: calendar_id || 'primary'
    });

    return items;
}

const createEvent = async (service, calendar_id, event) => {
    const { data } = await service.events.insert({
        calendarId: calendar_id || 'primary',
        resource: {
            ...event
        }
    });

    return data;
}

const deleteEvent = async (service, calendar_id, event_id) => {
    const data = await service.events.delete({
        calendarId: calendar_id,
        eventId: event_id
    });

    return data;
}

const updateEvent = async (service, calendar_id, event) => {
    const { data } = await service.events.update({
        calendarId: calendar_id,
        eventId: event.id,
        resource: {
            ...event
        }
    });

    return data;
}

//Calendar Helpers
const getCalendarList = async (service) => {
    const { data: { items } } = await service.calendarList.list();

    return items;
}

const createCalendar = async (service, calendar) => {
    const data = await service.calendars.insert({
        resource: {
            ...calendar
        }
    });

    return data;
}

const removeCalendar = async (service, calendar_id) => {
    const data = await service.calendars.delete({
        calendarId: calendar_id
    });

    return data;
}

const updateCalendar = async (service, calendar) => {
    const data = await service.calendars.update({
        calendarId: calendar.id,
        resource: {
            ...calendar
        }
    });

    return data;
}


//Events Controllers
export const getCalendarEventsController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    const { account_id } = event.pathParameters || {};
    const { platform_calendars, google_calendars, event_type } = event.body;

    /**
     *  platform_calendars: [ id1, id2, id3 ],
     *  google_calendars: {
     *      'abc@gmail.com': [calendarId1, calendarId2],
     *      'xyz@gmail.com': [calendarId2]
     *  }
     */

    const { error } = getEventSchema({ account_id, platform_calendars, google_calendars });
    if (error) return callback(null, response(400, error.details[0].message));

    let allEvents = {};

    if (platform_calendars.length && permissions[CAN_ACCESS_PLATFORM_CALENDARS]) {
        for (let calendar_id of platform_calendars) {
            let query = CalendarEvent.collectionRef.where('calendar_id', '==', calendar_id);
            if (event_type === 'appointment') query = query.where('type', '==', event_type);
            const snapshot = query.get();
            snapshot.docs.map(doc => allEvents[doc.id] = mapToTuiEvent({ id: doc.id, ...doc.data() }, `${calendar_id}:-:platform`));
        }
    }

    if (google_calendars && permissions[CAN_ACCESS_GOOGLE_CALENDARS]) {
        const accounts = Object.keys(google_calendars);

        for (const account of accounts) {
            const services = await commonChecks({ account_id, accounts: [account], apiId }, callback);
            for (const calendar_id of google_calendars[account]) {
                let events = await getEventsList(services[0], calendar_id);

                if (event_type === 'appointment') {
                    events.forEach(event => {
                        if (event.attendees.length)
                            allEvents[event.id] = mapToTuiEvent(event, `${calendar_id}:-:google`);
                    });
                } else {
                    events.forEach(event => allEvents[event.id] = mapToTuiEvent(event, `${calendar_id}:-:google`));
                }
            }
        }
    }

    callback(null, response(200, allEvents));
}

export const createCalendarEventController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    let { calendar_event, calendar_id, type, account_id, account } = event.body;
    calendar_event = mapToGoogleEvent(calendar_event);

    const { error } = saveEventSchema({ ...event, calendar_id, account_id, account }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    switch (type) {
        case 'platform':
            if (!permissions[CAN_ACCESS_PLATFORM_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            calendar_event['calendar_id'] = calendar_id;
            const doc = await CalendarEvent.add(calendar_event);

            calendar_event.id = doc.id;
            return callback(null, response(200, mapToTuiEvent(calendar_event, calendar_id)));
        case 'google':
            if (!permissions[CAN_ACCESS_GOOGLE_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            const services = await commonChecks({ account_id, accounts: [account], apiId }, callback);

            const { id } = await createEvent(services[0], calendar_id, event);

            calendar_event = mapToTuiEvent({ ...calendar_event, id }, calendar_id);
            callback(null, response(200, event));
        default:
            callback(null, response(400, ''));
    }
}

export const deleteCalendarEventController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    const { account_id } = event.pathParameters || {};
    const { calendar_id, event_id, type, account } = event.body;

    const { error } = deleteEventSchema({ calendar_id, event_id, account, account_id }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    switch (type) {
        case 'platform':
            if (!permissions[CAN_ACCESS_PLATFORM_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            await CalendarEvent.delete(event_id);
            return callback(null, response(200, event_id));
        case 'google':
            if (!permissions[CAN_ACCESS_GOOGLE_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            const services = await commonChecks({ account_id, accounts: [account], apiId }, callback);
            const result = await deleteEvent(services[0], calendar_id, event_id);
            return callback(null, response(200, result));
        default:
            callback(null, response(400, ''));
    }
}

export const updateCalendarEventController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    const { account_id } = event.pathParameters || {};
    let { calendar_event, calendar_id, from, to, type, account } = event.body;

    const { error } = updateEventSchema({ ...calendar_event, calendar_id, account_id, account, from, to }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    switch (type) {
        case 'platform':
            if (!permissions[CAN_ACCESS_PLATFORM_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            if (from && to) {
                await Promise.all([
                    CalendarEvent.delete(event.id),
                    CalendarEvent.update(event.id, { calendar_id: to })
                ]);

                event = mapToGoogleEvent(event);
                return callback(null, response(200, mapToTuiEvent(event, `${calendar_id}:-:platform`)));
            }

            await CalendarEvent.update(event.id, mapToGoogleEvent(event));
            event = mapToGoogleEvent(event);
            return callback(null, response(200, mapToTuiEvent(event, `${calendar_id}:-:platform`)));
        case 'google':
            if (!permissions[CAN_ACCESS_GOOGLE_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            const services = await commonChecks({ account_id, accounts: [account], apiId }, callback);

            const eventId = event.id || '';
            if (from && to) {
                event = mapToGoogleEvent(event);

                const { id } = await createEvent(services[0], to, event);
                await deleteEvent(services[0], from, eventId);

                event = mapToTuiEvent({ ...event, id }, to);
                return callback(null, response(200, event));
            }

            event = { id: event.id, ...mapToGoogleEvent(event) };
            await updateEvent(services[0], calendar_id, event);

            event = mapToTuiEvent(event, `${calendar_id}:-:google`);
            callback(null, response(200, event));
        default:
            callback(null, response(400, ''));
    }
}

//Calendar Controllers
export const getCalendarsController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    const { account_id } = event.pathParameters || {};
    let { accounts = '' } = event.queryStringParameters || {};

    accounts = accounts.split(',');

    const { error } = getCalendarSchema({ account_id, accounts });
    if (error) return callback(null, response(400, error.details[0].message));

    const calendars = {};

    if (accounts.length && permissions[CAN_ACCESS_GOOGLE_CALENDARS]) {
        const services = await commonChecks({ account_id, accounts, apiId }, callback);

        for (const service of services) {
            try {
                const list = await getCalendarList(service);
                list.forEach(calendar => calendars[`${calendar.id}:-:google`] = mapToTuiCalendar(calendar, 'google'));
            } catch (err) {
                callback(err);
            }
        }
    }

    if (permissions[CAN_ACCESS_PLATFORM_CALENDARS]) {
        const platformCalendars = await Calendar.collectionRef.where(CalendarModel.ACCOUNT_ID, '==', account_id).get();
        platformCalendars.docs.map(doc => calendars[`${doc.id}:-:platform`] = mapToTuiCalendar({ ...doc.data(), id: doc.id }, 'platform'));
    }

    callback(null, response(200, calendars));
}

export const createNewCalendarController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    let { calendar, type, account_id, account } = event.body;

    calendar[CalendarModel.ACCOUNT_ID] = account_id;

    const { error } = saveCalendarSchema({ ...calendar, account }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    switch (type) {
        case 'platform':
            if (!permissions[CAN_ACCESS_PLATFORM_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            calendar = {
                ...calendar,
                ...getCalendarColor()
            }
            const doc = await Calendar.add(calendar);

            calendar.id = doc.id;
            return callback(null, response(200, mapToTuiCalendar(calendar, 'platform')));
        case 'google':
            if (!permissions[CAN_ACCESS_GOOGLE_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            const services = await commonChecks({ account_id, accounts: [account], apiId }, callback);

            let { data } = await createCalendar(services[0], calendar);

            return callback(null, response(200, mapToTuiCalendar(data, 'google')));
        default:
            callback(null, response(400, ''));
    }
}

export const deleteCalendarController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    const { account_id } = event.pathParameters || {};
    const { calendar_id, type, account } = event.body;

    const { error } = deleteCalendarSchema({ calendar_id, account_id, account }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    switch (type) {
        case 'platform':
            if (!permissions[CAN_ACCESS_PLATFORM_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            const events = await CalendarEvent.collectionRef.where(CalendarEventModel.CALENDAR_ID, '==', calendar_id).get();

            await Promise.all([
                Calendar.delete(calendar_id),
                CalendarEvent.deleteMultipleDoc([''], events)
            ]);

            return callback(null, response(200, calendar_id));
        case 'google':
            if (!permissions[CAN_ACCESS_GOOGLE_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            const accounts = [account];
            const services = await commonChecks({ account_id, accounts, apiId }, callback);
            const result = await removeCalendar(services[0], calendar_id);

            return callback(null, response(200, result));
        default:
            callback(null, response(400, ''));
    }
}

export const updateCalendarInfoController = async (event, callback) => {
    const { permissions, requestContext: { apiId } } = event;

    const { account_id } = event.pathParameters || {};
    const { calendar, type, account } = event.body;

    const { error } = updateCalendarSchema({ ...calendar, account, account_id }, type);
    if (error) return callback(null, response(400, error.details[0].message));

    let services;

    switch (type) {
        case 'platform':
            if (!permissions[CAN_ACCESS_PLATFORM_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            const { id } = calendar;
            await Calendar.update(id, calendar);
            return callback(null, response(200, mapToTuiCalendar(calendar, 'platform')));
        case 'google':
            if (!permissions[CAN_ACCESS_GOOGLE_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            services = await commonChecks({ account_id, accounts: [account], apiId }, callback);
            await updateCalendar(services[0], calendar);
            callback(null, response(200, mapToTuiCalendar(calendar, 'google')));
        case 'move_to_google':
            if (!permissions[CAN_ACCESS_GOOGLE_CALENDARS])
                return callback(null, response(403, 'Access denied. You are not authorized.'));
            services = await commonChecks({ account_id, accounts: [account], apiId }, callback);
            const events = await CalendarEvent.collectionRef.where('calendar_id', '==', calendar.id).get();
            const calendarId = calendar.id;
            delete calendar.id;
            let { data } = await createCalendar(services[0], calendar);

            const allEvents = {};
            for (let event of events.docs) {
                event = event.data();
                delete event.calendar_id;
                const { id } = await createEvent(services[0], data.id, event);
                event = mapToTuiEvent({ ...event, id }, `${data.id}:-:google`);

                allEvents[event.id] = event;
            }

            await Promise.all([
                Calendar.delete(calendarId),
                CalendarEvent.deleteMultipleDoc([''], events)
            ]);

            return callback(null, response(200, { calendar: mapToTuiCalendar(data, 'google'), events: allEvents }));
        default:
            callback(null, response(400, ''))
    }
}