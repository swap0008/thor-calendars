import {
    getCalendarEventsController,
    createCalendarEventController,
    deleteCalendarEventController,
    updateCalendarEventController
} from './calendars.js';

export const getAppointmentsController = (event, callback) => { //event_type: appointment
    getCalendarEventsController(event, callback);
}

export const createAppointmentController = (event, callback) => {
    createCalendarEventController(event, callback);
}

export const deleteAppointmentController = (event, callback) => {
    deleteCalendarEventController(event, callback);
}

export const updateAppointmentController = (event, callback) => {
    updateCalendarEventController(event, callback);
}