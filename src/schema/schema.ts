import * as Joi from '@hapi/joi';
import { CalendarModel } from '../models/CalendarModel.js';
import { CalendarEventModel } from '../models/CalendarEventModel.js';

const getCalendarFields = () => {
    return {
        [CalendarModel.SUMMARY]: Joi.string(),
        [CalendarModel.DESCRIPTION]: Joi.string().allow(''),
        [CalendarModel.ACCOUNT_ID]: Joi.string()
    };
};

const getEventsFields = () => {
    return {
        [CalendarEventModel.SUMMARY]: Joi.string(),
        [CalendarEventModel.DESCRIPTION]: Joi.string().allow(''),
        [CalendarEventModel.LOCATION]: Joi.string().allow(''),
        [CalendarEventModel.END]: Joi.date(),
        [CalendarEventModel.START]: Joi.date(),
        [CalendarEventModel.CALENDAR_ID]: Joi.string(),
        [CalendarEventModel.ATTENDEES]: Joi.array().items(Joi.object({
            displayName: Joi.string(),
            email: Joi.string().email()
        }))
    }
}

export const getEventSchema = (data: object) => {
    const schema = Joi.object({
        'account_id': Joi.string().required(),
        'platform_calendars': Joi.array().items(Joi.string()),
        'google_calendars': Joi.object()
    });

    return schema.validate(data);
}

export const saveEventSchema = (data: object, type: string) => {
    const fields = getEventsFields();
    let schema = {
        [CalendarEventModel.SUMMARY]: fields[CalendarEventModel.SUMMARY].required(),
        [CalendarEventModel.DESCRIPTION]: fields[CalendarEventModel.DESCRIPTION],
        [CalendarEventModel.LOCATION]: fields[CalendarEventModel.LOCATION],
        [CalendarEventModel.END]: fields[CalendarEventModel.END].required(),
        [CalendarEventModel.START]: fields[CalendarEventModel.START].required(),
        [CalendarEventModel.CALENDAR_ID]: fields[CalendarEventModel.CALENDAR_ID].required(),
        [CalendarEventModel.ATTENDEES]: fields[CalendarEventModel.ATTENDEES]
    };
    switch (type) {
        case 'platform':
            schema = Joi.object({
                ...schema
            });
        case 'google':
            schema = Joi.object({
                ...schema,
                'account': Joi.string().email().required(),
                'account_id': Joi.string().required()
            })
    }

    return schema.validate(data);
}

export const deleteEventSchema = (data: object, type: string) => {
    let schema;
    switch (type) {
        case 'platform':
            schema = Joi.object({
                'event_id': Joi.string().required()
            });
        case 'google':
            schema = Joi.object({
                'account': Joi.string().email().required(),
                'account_id': Joi.string().required(),
                'calendar_id': Joi.string().required(),
                'event_id': Joi.string().required()
            });
    }

    return schema.validate(data);
}

export const updateEventSchema = (data: object, type: string) => {
    const fields = getEventsFields();
    let schema = {
        [CalendarEventModel.SUMMARY]: fields[CalendarEventModel.SUMMARY],
        [CalendarEventModel.DESCRIPTION]: fields[CalendarEventModel.DESCRIPTION],
        [CalendarEventModel.LOCATION]: fields[CalendarEventModel.LOCATION],
        [CalendarEventModel.END]: fields[CalendarEventModel.END],
        [CalendarEventModel.START]: fields[CalendarEventModel.START],
        [CalendarEventModel.CALENDAR_ID]: fields[CalendarEventModel.CALENDAR_ID],
        id: Joi.string().required(),
        from: Joi.string(),
        to: Joi.string()
    };
    switch (type) {
        case 'platform':
            schema = Joi.object({
                ...schema
            });
        case 'google':
            schema = Joi.object({
                ...schema,
                'account': Joi.string().email().required(),
                'account_id': Joi.string().required()
            })
    }

    return schema.validate(data);
}

export const getCalendarSchema = (data: object) => {
    const schema = Joi.object({
        'account_id': Joi.string().required(),
        'accounts': Joi.array().items(Joi.string().email())
    });

    return schema.validate(data);
}

export const saveCalendarSchema = (data: object, type: string) => {
    const fields = getCalendarFields();
    let schema = {
        [CalendarModel.SUMMARY]: fields[CalendarModel.SUMMARY].required(),
        [CalendarModel.DESCRIPTION]: fields[CalendarModel.DESCRIPTION],
        [CalendarModel.ACCOUNT_ID]: fields[CalendarModel.ACCOUNT_ID].required()
    }

    switch (type) {
        case 'platform':
            schema = Joi.object(schema);
        case 'google':
            schema = Joi.object({
                ...schema,
                account: Joi.string().email().required()
            });
        default:
            schema = Joi.object({});
    }

    return schema.validate(data);
}

export const deleteCalendarSchema = (data: object, type: string) => {
    let schema;

    switch (type) {
        case 'platform':
            schema = Joi.object({
                'calendar_id': Joi.string().required()
            });
        case 'google':
            schema = Joi.object({
                'calendar_id': Joi.string().required(),
                account: Joi.string().email().required(),
                'account_id': Joi.string().required()
            });
        default:
            schema = Joi.object({});
    }

    return schema.validate(data);
}

export const updateCalendarSchema = (data: object, type: string) => {
    const fields = getCalendarFields();
    let schema = {
        [CalendarModel.SUMMARY]: fields[CalendarModel.SUMMARY],
        [CalendarModel.DESCRIPTION]: fields[CalendarModel.DESCRIPTION],
        [CalendarModel.ACCOUNT_ID]: fields[CalendarModel.ACCOUNT_ID].required()
    }

    switch (type) {
        case 'platform':
            schema = Joi.object({
                'id': Joi.string().required(),
                ...schema
            });
        case 'google':
            schema = Joi.object({
                ...schema,
                account: Joi.string().email().required(),
                'account_id': Joi.string().required()
            });
        case 'move_to_google':
            schema = Joi.object({
                account: Joi.string().email().required(),
                'account_id': Joi.string().required(),
                'id': Joi.string().required()
            })
        default:
            schema = Joi.object({});
    }

    return schema.validate(data);
}