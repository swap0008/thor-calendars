import { applyMiddleware, verifyJwtToken } from 'middlewares-nodejs';
import {
    getAppointmentsController,
    createAppointmentController,
    deleteAppointmentController,
    updateAppointmentController
} from '../controller/appointments.js';

export const healthCheck = (event, context, callback) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify(`You're being served from ap-south-1 Mumbai region. (SchedulingAPI)`)
    }

    callback(null, response);
}

export const getAppointments = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], getAppointmentsController);
}

export const createAppointment = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], createAppointmentController);
}

export const deleteAppointment = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], deleteAppointmentController);
}

export const updateAppointment = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], updateAppointmentController);
}