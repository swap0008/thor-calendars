import { applyMiddleware, verifyJwtToken } from 'middlewares-nodejs';
import {
    getCalendarsController,
    createNewCalendarController,
    deleteCalendarController,
    updateCalendarInfoController,
    getCalendarEventsController,
    createCalendarEventController,
    deleteCalendarEventController,
    updateCalendarEventController
} from '../controller/calendars.js';

export const getCalendars = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], getCalendarsController);
}

export const createCalendar = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], createNewCalendarController);
}

export const deleteCalendar = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], deleteCalendarController);
}

export const updateCalendar = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], updateCalendarInfoController);
}

export const getCalendarEvents = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], getCalendarEventsController);
}

export const createCalendarEvent = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], createCalendarEventController);
}

export const deleteCalendarEvent = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], deleteCalendarEventController);
}

export const updateCalendarEvent = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken], updateCalendarEventController);
}