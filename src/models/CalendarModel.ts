import { FirestoreClient } from './FirestoreClient.js';

export class CalendarModel extends FirestoreClient {
    static SUMMARY = 'summary';
    static DESCRIPTION = 'description';
    static ACCOUNT_ID = 'account_id';

    constructor() {
        super('calendar');
    }
}