import { FirestoreClient } from './FirestoreClient.js';

export class CalendarEventModel extends FirestoreClient {
    static SUMMARY = 'summary';
    static DESCRIPTION = 'description';
    static LOCATION = 'location';
    static END = 'end';
    static START = 'start';
    static CALENDAR_ID = 'calendar_id';
    static ATTENDEES = 'attendees';

    constructor() {
        super('calendar_events');
    }
}
